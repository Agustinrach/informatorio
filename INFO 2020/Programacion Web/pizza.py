
#Ingredientes vegetarianos: Pimiento y Rúcula  .
#Ingredientes no vegetarianos: Jamón y Panceta. 
#Escribir un programa que pregunte al usuario si quiere una pizza vegetariana o no, y 
#en función de su respuesta le muestre un menú con los ingredientes disponibles para que elija. 
#Solo se puede elegir un ingrediente además de la mozzarella y el tomate que están en todas la pizzas. 
#Al final se debe 
#mostrar por pantalla si la pizza elegida es vegetariana o no y todos los ingredientes que lleva
ingredientes_generales = "mozarella, tomate,"
ingredientes_vegetarianos = "pimineto y rucula"
ingredientes_carnivoros = "jamon y panceta"
eleccion = str(input("Elija su pizza: 'V' para vegana y 'C' para carnivora: "))
if eleccion == "V":
	print("PIZZA VEGETARIANA:", ingredientes_generales,ingredientes_vegetarianos)
elif eleccion == "C":
	print("PIZZA CARNIVORA:",ingredientes_generales, ingredientes_carnivoros)
else: print("ERROR")