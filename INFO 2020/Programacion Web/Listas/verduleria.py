def imprimir():
	for i in frutas:
		print(i)

	print("\n")

	for i in verduras:
		print(i)

frutas = ["Manzana","Banana","Pera","Naranja"]
verduras = ["Papa","Zapallo","Batata",["Acelga","Rucula","Lechuga","Espinaca"]]
frutas_copia= frutas.copy() 
imprimir()
print("\n")
frutas.append(["tomate","sandia","mango"])
verduras.append("remolacha")
imprimir()
print("\n")
frutas.insert(0,"Frutilla")
verduras.insert(2,"Morron")
imprimir()
print("\n")
verduras.extend(frutas)
print(verduras)
print("\n")

if "Pera" in verduras:
	indice = verduras.index("Pera")
	print(indice)

verduras_recortadas = verduras[:4]	
print(verduras_recortadas)
print(frutas_copia)
frutas_copia.sort()
print(frutas_copia)
del frutas_copia[3]
print(frutas_copia)
frutas_copia.remove("Manzana")
print(frutas_copia)